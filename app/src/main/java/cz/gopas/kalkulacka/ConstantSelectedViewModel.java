package cz.gopas.kalkulacka;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ConstantSelectedViewModel extends ViewModel {
	public final MutableLiveData<ConstantEntity> constant = new MutableLiveData<>();
}
