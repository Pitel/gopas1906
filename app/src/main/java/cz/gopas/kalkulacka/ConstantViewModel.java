package cz.gopas.kalkulacka;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.room.Room;

import java.util.List;

public class ConstantViewModel extends AndroidViewModel {
	private final ConstantDatabase db;

	public ConstantViewModel(@NonNull Application application) {
		super(application);
		db = Room.databaseBuilder(getApplication(), ConstantDatabase.class, "constants")
				.allowMainThreadQueries()
				.build();
		db.constantDao().insertAll(
				new ConstantEntity("Pi", Math.PI),
				new ConstantEntity("e", Math.E),
				new ConstantEntity("The Answer", 42)
		);
	}

	public List<ConstantEntity> getConstants() {
		return db.constantDao().getConstants();
	}
}
