package cz.gopas.kalkulacka;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {ConstantEntity.class}, version = 1)
public abstract class ConstantDatabase extends RoomDatabase {
	public abstract ConstantDao constantDao();
}