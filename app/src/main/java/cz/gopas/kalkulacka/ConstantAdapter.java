package cz.gopas.kalkulacka;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class ConstantAdapter extends ListAdapter<ConstantEntity, ConstantAdapter.ConstantViewHolder> {
	private static final String TAG = ConstantAdapter.class.getSimpleName();

	private final ConstantClickListener onClick;

	public ConstantAdapter(ConstantClickListener onClick) {
		super(DIFF_CALLBACK);
		this.onClick = onClick;
	}

	@NonNull
	@Override
	public ConstantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		Log.d(TAG, "Create");
		final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		final View view = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);
		return new ConstantViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull ConstantViewHolder holder, int position) {
		Log.v(TAG, "Bind");
		final ConstantEntity constant = getItem(position);
		holder.name.setText(constant.name);
		holder.value.setText(String.valueOf(constant.value));
		holder.itemView.setOnClickListener(view -> onClick.onClick(constant));
	}

	private static final DiffUtil.ItemCallback<ConstantEntity> DIFF_CALLBACK = new DiffUtil.ItemCallback<ConstantEntity>() {
		@Override
		public boolean areItemsTheSame(@NonNull ConstantEntity old, @NonNull ConstantEntity new_) {
			return old == new_;
		}

		@Override
		public boolean areContentsTheSame(@NonNull ConstantEntity old, @NonNull ConstantEntity new_) {
			// NOTE: if you use equals, your object must properly override Object#equals()
			// Incorrectly returning false here will result in too many animations.
			return old.name.equals(new_.name);
		}
	};

	class ConstantViewHolder extends RecyclerView.ViewHolder {
		public TextView name, value;

		public ConstantViewHolder(@NonNull View itemView) {
			super(itemView);
			name = itemView.findViewById(android.R.id.text1);
			value = itemView.findViewById(android.R.id.text2);
		}
	}

	public interface ConstantClickListener {
		void onClick(ConstantEntity constant);
	}
}
