package cz.gopas.kalkulacka;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ConstantFragment extends Fragment {
	private static final String TAG = ConstantFragment.class.getSimpleName();
	private ConstantViewModel viewModel;
	private ConstantSelectedViewModel constantSelectedViewModel;

	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);
		viewModel = ViewModelProviders.of(this).get(ConstantViewModel.class);
		constantSelectedViewModel = ViewModelProviders.of(requireActivity()).get(ConstantSelectedViewModel.class);
		for (ConstantEntity c : viewModel.getConstants()) {
			Log.d(TAG, c.name);
		}
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_constant, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		final RecyclerView recycler = (RecyclerView) view;
		recycler.setHasFixedSize(true);
		final ConstantAdapter adapter = new ConstantAdapter(constant -> {
			Log.i(TAG, constant.name);
			constantSelectedViewModel.constant.setValue(constant);
			requireFragmentManager().popBackStack();
		});
		recycler.setAdapter(adapter);
		final List<ConstantEntity> constants = viewModel.getConstants();
		for (int i = 0; i < 1000; i++) {
			constants.add(new ConstantEntity(String.valueOf(i), i));
		}
		adapter.submitList(constants);
	}
}
