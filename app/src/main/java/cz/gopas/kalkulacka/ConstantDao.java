package cz.gopas.kalkulacka;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ConstantDao {
	@Query("SELECT * from constantentity")
	List<ConstantEntity> getConstants();

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void insertAll(ConstantEntity... constants);
}
