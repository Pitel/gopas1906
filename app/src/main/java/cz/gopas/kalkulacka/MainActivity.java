package cz.gopas.kalkulacka;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.gopas.kalkulacka.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

	private static final String TAG = MainActivity.class.getSimpleName();



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		FragmentManager.enableDebugLogging(BuildConfig.DEBUG);

		Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "onCreate");

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.replace(android.R.id.content, new CalcFragment())
					.commit();
		}

		try {
			Log.i(TAG, getIntent().getDataString());
		} catch (Throwable t) {
			//Log.w(TAG, t);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "onResume");
	}

	@Override
	protected void onPause() {
		Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "onPause");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "onStop");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Toast.makeText(this, "Destroy", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}
}
