package cz.gopas.kalkulacka;

import android.app.Application;
import android.util.Log;

import androidx.annotation.IdRes;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.concurrent.TimeUnit;

public class CalcViewModel extends AndroidViewModel {

	private MutableLiveData<Float> _result = new MutableLiveData<>();
	public LiveData<Float> result = _result;

	public CalcViewModel(Application app) {
		super(app);
	}

	public void calc(final float a, @IdRes final int op, final float b) {
		new Thread(() -> {
			final float x;
			switch (op) {
				case R.id.add:
					x = a + b;
					break;
				case R.id.sub:
					x = a - b;
					break;
				case R.id.mul:
					x = a * b;
					break;
				case R.id.div:
					x = a / b;
					break;
				default:
					x = Float.NaN;
			}
			try {
				Log.d(Thread.currentThread().getName(), "3");
				Thread.sleep(TimeUnit.SECONDS.toMillis(1));
				Log.d(Thread.currentThread().getName(), "2");
				Thread.sleep(TimeUnit.SECONDS.toMillis(1));
				Log.d(Thread.currentThread().getName(), "1");
				Thread.sleep(TimeUnit.SECONDS.toMillis(1));
				_result.postValue(x);
			} catch (InterruptedException e) {
				// Interrupted
			}
		}, "calcThread").start();
	}

}
