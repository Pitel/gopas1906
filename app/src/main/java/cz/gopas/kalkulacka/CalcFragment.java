package cz.gopas.kalkulacka;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cz.gopas.kalkulacka.databinding.ActivityMainBinding;

public class CalcFragment extends Fragment {
	private ActivityMainBinding binding;
	private Unbinder unbinder;

	private static final String TAG = CalcFragment.class.getSimpleName();

	private SharedPreferences prefs;

	private CalcViewModel viewModel;
	private ConstantSelectedViewModel constantViewModel;

	public CalcFragment() {
		setHasOptionsMenu(true);
	}

	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		viewModel = ViewModelProviders.of(this).get(CalcViewModel.class);
		constantViewModel = ViewModelProviders.of(requireActivity()).get(ConstantSelectedViewModel.class);
	}

	@Override
	public void onStart() {
		super.onStart();
		((AppCompatActivity) requireActivity()).getSupportActionBar().setTitle(R.string.app_name);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		binding = DataBindingUtil.inflate(inflater, R.layout.activity_main, container, false);
		binding.executePendingBindings();
		return binding.getRoot();
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		//binding.calc.setOnClickListener(v -> calc());
		viewModel.result.observe(getViewLifecycleOwner(), result -> {
			binding.setResult(String.valueOf(result));
			binding.share.setVisibility(View.VISIBLE);
			prefs.edit().putString("ans", String.valueOf(result)).apply();
		});
		constantViewModel.constant.observe(getViewLifecycleOwner(), constant -> {
			if (constant != null) {
				binding.setA(String.valueOf(constant.value));
				constantViewModel.constant.setValue(null);
			}
		});
		unbinder = ButterKnife.bind(this, view);
	}

	@Override
	public void onDestroyView() {
		unbinder.unbind();
		super.onDestroyView();
	}

	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
		inflater.inflate(R.menu.menu, menu);
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		switch (item.getItemId()) {
			case R.id.about:
				Log.d(TAG, "About");
				requireFragmentManager().beginTransaction()
						.replace(android.R.id.content, new AboutFragment())
						.addToBackStack(null)
						.commit();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	/*
	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("res", binding.getResult());
	}

	@Override
	public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
		try {
			binding.setResult(savedInstanceState.getString("res", ""));
		} catch (NullPointerException npe) {
			// First start
		}
	}
	 */

	@OnClick(R.id.calc)
	void calc() {
		Log.d(TAG, "Calc!");
		float a, b;
		try {
			a = Float.parseFloat(binding.getA());
		} catch (NumberFormatException | NullPointerException e) {
			a = Float.NaN;
		}
		try {
			b = Float.parseFloat(binding.getB());
		} catch (NumberFormatException | NullPointerException e) {
			b = Float.NaN;
		}
		viewModel.calc(a, binding.operation.getCheckedRadioButtonId(), b);
	}

	@OnClick(R.id.ans)
	void ans() {
		binding.setB(prefs.getString("ans", ""));
	}

	@OnClick(R.id.constant)
	void constant() {
		requireFragmentManager().beginTransaction()
				.replace(android.R.id.content, new ConstantFragment())
				.addToBackStack(null)
				.commit();
	}

	@OnClick(R.id.share)
	void share() {
		final Intent intent = new Intent(Intent.ACTION_SEND)
				.setType("text/plain")
				.putExtra(Intent.EXTRA_TEXT, getString(R.string.result, binding.getResult()));
		startActivity(intent);
	}
}
