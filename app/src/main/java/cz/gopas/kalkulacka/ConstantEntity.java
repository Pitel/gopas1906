package cz.gopas.kalkulacka;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ConstantEntity {
	public ConstantEntity(@NonNull String name, double value) {
		this.name = name;
		this.value = value;
	}

	@PrimaryKey
	@NonNull
	public String name;

	public double value;
}
